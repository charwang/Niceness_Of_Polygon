import math

import graphics as gp


def pass_list(l):
    return l

def draw_polygon_from_list(l):
    poly = gp.Polygon(l)
    poly.gp.setOutline(gp.color_rgb(0, 255, 255))
    poly.setFill(gp.color_rgb(176,224,230))
    poly.draw(win)

def main():
    global win
    win = gp.GraphWin("Niceness of Polygon first prototype", 500, 500)
    win.setBackground(gp.color_rgb(0,0,0))

    # poly = Polygon(Point(0, 350), Point(350, 350), Point(50, 150))


    poly_arr = []
        #[Point(193.0, 262.0), Point(341.0, 181.0), Point(305.0, 294.0), Point(212.0, 364.0), Point(186.0, 299.0)]

    #draw an input polygon
    while True:
        i = win.checkKey()
        if i == "Return":
            print("enter key")
            break
        else:
            mouse = win.getMouse()
            poly_arr.append(mouse)
            mouse.setOutline(gp.color_rgb(255,255,0))
            mouse.draw(win)

    poly = gp.Polygon(poly_arr)
    poly.setOutline(gp.color_rgb(0, 255, 255))
    poly.draw(win)

    #draw a rectangle
    rec_arr = []
    for i in range(0,2):
        mouse = win.getMouse()
        rec_arr.append(mouse)

    rec = gp.Rectangle(rec_arr[0],rec_arr[1])
    rec.setOutline(gp.color_rgb(0, 255, 255))
    rec.draw(win)

    pass_list(poly_arr)
    pass_list(rec_arr)

    # draw_polygon_from_list([Point(5, 10), Point(34, 18), Point(30, 29), Point(21, 36), Point(18, 29)])
    area = 8.0
    area_str = "intersection area " + str(area)
    txt = gp.Text(gp.Point(250,50), area_str)
    txt.setTextColor(gp.color_rgb(0,255,200))
    txt.setSize(20)
    txt.draw(win)

    if win.getKey()== "Return":
        win.close()

win = None
main()